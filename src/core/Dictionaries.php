<?php


namespace eqbSaveEvidence\core;
use eqbSaveEvidence\utils\Utils;

class Dictionaries
{


    /**
     * 创建行业
     *
     * @param array $param  example $param = ['name' => ["房屋租赁行业"]];
     * @return array    include $busId
     */
    public static function createBusiness(array $param)
    {
        $pa = json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        //发送http请求
        return  Utils::httpPostData($pa, $signature, PROJECT_ID , BUS_ADD_API);
    }

    /**
     * 创建业务凭证
     * @param $busId
     * @param array $scenes example $scenes = ["房屋租赁合同签署"]
     * @return array
     */
    public static function createScene($busId,array $scenes)
    {
        //设置业务凭证参数
        $param = [
            "businessTempletId"=>$busId,
            'name' => $scenes
        ];
        $pa = json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , SCENE_ADD_API);
    }

    /**
     * 创建证据点
     * @param $sceneId   业务凭证ID
     * @param $names      证据点名称 example $names = ['证据点1'];
     * @return array
     */
    public static function createSeg($sceneId,$names)
    {
        $param = [
            "sceneTempletId"=>$sceneId,
            "name"=>$names
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , SEG_ADD_API);
    }


    /**
     * 创建证据点字段属性
     * @param $segId
     * @param array $properties  example $properties = [['displayName'=>'甲方姓名','realName'=>'张三'],['displayName'=>'甲方姓名','realName'=>'张三']];
     * @return array
     */
    public static function createSegProp($segId,array $properties)
    {
        $param = [
          "segmentTempletId"=>$segId,
          "properties"=>$properties
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , SEGPROP_ADD_API);
    }



}