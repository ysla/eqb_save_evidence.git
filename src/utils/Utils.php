<?php


namespace eqbSaveEvidence\utils;


class Utils
{

    /**
     * @param array $data
     * @param string $signature
     * @param string $projectId
     * @param string $url
     * @return array
     */
    public static function httpPostData(array $data,string $signature,string $projectId,string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-timevale-mode:package", "X-timevale-project-id:" . $projectId, "X-timevale-signature:" . $signature, "X-timevale-signature-algorithm:hmac-sha256", "Content-Type:application/json"));
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array($return_code, $return_content);
    }


    /**
     * @param $url
     * @param $contentBase64Md5
     * @param $fileContent
     * @return int|mixed
     */
    public static function sendHttpPut($url, $contentBase64Md5, $fileContent)
    {
        $header = [
            'Content-Type:application/octet-stream',
            'Content-Md5:' . $contentBase64Md5
        ];

        $status = '';
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_FILETIME, true);
        curl_setopt($curl_handle, CURLOPT_FRESH_CONNECT, false);
        curl_setopt($curl_handle, CURLOPT_HEADER, true); // 输出HTTP头 true
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 5184000);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');

        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fileContent);
        $result = curl_exec($curl_handle);
        $status = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

        if ($result === false) {
            $status = curl_errno($curl_handle);
            $result = 'put file to oss - curl error :' . curl_error($curl_handle);
        }
        curl_close($curl_handle);
//    $this->debug($url, $fileContent, $header, $result);
        return $status;
    }


    /**
     * 获取文件的Content-MD5
     * 原理：1.先计算MD5加密的二进制数组（128位）。
     * 2.再对这个二进制进行base64编码（而不是对32位字符串编码）。
     * @param $filePath
     * @return string
     */
    public static function getContentBase64Md5($filePath)
    {
        $md5file = md5_file($filePath,true);
        return base64_encode($md5file);
    }

    /**
     * 计算文件的sha256
     * @param $filePath
     * @return string
     */
    public static function getFileSha256($filePath)
    {
        return  hash_file('sha256',$filePath,false);
    }

    /**
     * 计算请求签名值
     * @param $message
     * @param $projectSecret
     * @return string
     */
    public static function getSignature($message, $projectSecret)
    {
        return hash_hmac('sha256', $message, $projectSecret, FALSE);
    }



    /**
     * 获取当前时间戳（毫秒级）
     * @return float
     */
    public static function getMillisecond()
    {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}